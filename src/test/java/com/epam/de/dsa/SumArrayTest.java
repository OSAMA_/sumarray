package com.epam.de.dsa;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SumArrayTest {

    SumArray sumArray;

    @BeforeEach
    void setUp(){
        sumArray = new SumArray();
    }

    @Test
    void sumTest(){
        int[] arr = {1,2,3,4,5};
        Assertions.assertEquals(15,sumArray.sum(arr));
        Assertions.assertEquals(15,sumArray.sum(arr,5));
    }

}