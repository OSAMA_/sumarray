package com.epam.de.dsa;

public class SumArray {
    public int sum(int[] elements) {
        int result = 0;
        for (int element : elements) {
            result += element;
        }

        return result;
    }

    //Provide iterative solution
    public int sum(int[] elements, int size) {
        if(size == 0){
            return 0;
        }
        int lastElement = elements[size-1];
        return lastElement + sum(elements, --size);
    }
}
